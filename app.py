from flask import Flask, render_template, request, redirect, url_for
import smtplib
from threading import Thread

app = Flask(__name__)

carriers = {'AT&T' : '@text.att.net', 'Verizon' : '@vtext.net', 'Tmobile' : '@tmombail.net', 'Sprint PCS' : '@messaging.sprintpcs.com', 'Virgin Mobile' : '@vmobl.com', 'US Cellular' : '@email.uscc.net', 'Nextel' : '@messaging.nextel.com', 'Boost' : '@myboostmobile.com', 'Alltell' : '@message.alltell.com'}

# async spam deployer
def smspam_async(username,password,sendto,message,times):
    mailserver = smtplib.SMTP('smtp.gmail.com', 587)
    mailserver.ehlo()
    mailserver.starttls()
    mailserver.ehlo()
    mailserver.login(username,password)
    for x in range(0,times):
        mailserver.sendmail(username, sendto, message)
    mailserver.close()

# spamming thread wrapper
def smspam(username,password,sendto,message,times):
    thr = Thread(target=smspam_async, args=[username,password,sendto,message,times])
    thr.start()

# main page
@app.route('/')
def index():
    return render_template('index.html', carriers=carriers)

# spamming page
# TODO: Json support
@app.route('/spam', methods=['GET', 'POST'])
def spam():
    if request.method == 'POST':
        try:
            username = request.form['gmailUser']
            password = request.form['gmailPass']
            phone = request.form['phoneNum']
            carr = request.form['carrier']
            message = request.form['textMsg']
            times = int(request.form['timesNum'])
            sendto = phone + carriers[carr]
            smspam(username,password,sendto,message,times)
            #print "success"
        except KeyError:
            return redirect(url_for('index')) 
    return redirect(url_for('index'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
