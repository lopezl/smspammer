# SMSpammer #

This is a flask based SMSpammer which is simple and fast.

### What is this for? ###

Proof of concept code for attacking SMS to email gateways, also could be used to create a bridge for free texting. **USE AT YOUR OWN RISK.**

### How do I get set up? ###

1. Install virtualenv and pip `sudo apt-get install python-virtualenv python-pip`
2. Clone this repo `git clone <clone address>`
3. Go into repo and create a virtualenv
4. Activate the virtualenv, and install flask `pip install Flask`
5. Run the program `python app.py`

### License ###
[AGPLv3](https://www.gnu.org/licenses/agpl.html)
